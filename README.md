In this example we'll asume 4 nodes: `RGB`, `Depth`, `Normals` and `Semantic`

Step 1) Train single links model on train set, validate on validation set (i.e. rgb->depth_gt, rgb->normals_gt, rgb->semantic_gt)
  - create a datset under datasets/some_dataset/train_set (and validation_set) with 4 folders: `depth_gt`, `normals_gt`, `semantic_gt`, `rgb`

This can be done via `main_train_sl_from_gt.py` or manually as long as you get the model checkpoints for each single link you care about.

```
 python main_train_sl_from_gt.py --graph_cfg_path /path/to/graph.yaml --train_cfg_path config/train.yaml --graph_nodes_path /path/to/nodes_implementation.py --train_set_path datasets/some_dataset/train_set/ --validation_set_path datasets/some_dataset/validation_set/ --experiment_name some_name
```

- graph.yaml path in under config/graph/*
- /path/to/nodes_implementation.py is the path to where the nodes are defined. I'm using [this](https://gitlab.com/neural-graph-consensus/ngc-video-scene-understanding), so clone that repo and point to `main.py` to load the nodes/models.

Step 2) Export predictions of these single links

- This can be done via [link](https://gitlab.com/neural-graph-consensus/ngc-video-scene-understanding/-/blob/master/main_export_predictions.py) or manually
- Store them under `datasets/some_dataset/train_set/*_pred` i.e `semantic_pred`, `depth_pred`, `normals_pred` etc.

Step 3) Train reverse link using `main_train_rsl_from_predictions.py` [depth_pred + normals_pred + semantic_pred] -> rgb
  - careful for semantic to main_train_rsl_from_predictions.py in [0-1], thus predictions are stored main_train_rsl_from_predictions.py

```
python main_train_rsl_from_predictions.py --graph_cfg_path /path/to/graph_pred.yaml --train_cfg_path config/train.yaml  --graph_nodes_path ../ngc-video-scene-understanding/main.py  --train_set_path datasets/some_dataset/train_set/ --validation_set_path datasets/some_dataset/validation_set/

```

Step 4) (optional, you can work on the original dataset)
 - create a new dataset with the same structure
 ```
 datasets/
   new_dataset/
    rgb/
      0.npz, .., N.npz
    depth_gt/
    depth_pred/
    semantic_gt/
    semantic_pred/
    normals_gt/
    normals_pred/
 ```

Step 5) Iteratively reconstruct the input 
  - use `main_grad_input.py` to export the plots under some directory `plots/some_new_dataset/frame XX/*.pngg`

  ```
  python main_grad_input.py  --graph_cfg_path config/optim/rAll.yaml --graph_nodes_path ../ngc-video-scene-understanding/main.py --weights_path lightning_logs/rAll/checkpoints/epoch\=99-step\=18099.ckpt --dataset_path datasets/new_dataset/ --output_dir plots/new_dataset --max_iters 200 --lr 0.05
  ```

Step 6)
- Make video collage on 1st, 2nd and last iteration via `main_make_collage.py` on the exported plots directory
- Make figure plots on the losses using `main_get_loss_plot.py` on the exported plots directory

Das it.
