"""
Converts the exported loss.npz via main_grad_input.py for each frame to a plot showing 3 bars: first, 2nd and last
losses for all input nodes and the reconstructed node (rgb).
"""
import numpy as np
import matplotlib.pyplot as plt
from pathlib import Path
from argparse import ArgumentParser
from natsort import natsorted

def get_args():
    parser = ArgumentParser()
    parser.add_argument("--base_dir", required=True)
    parser.add_argument("--title", required=True)
    args = parser.parse_args()
    args.base_dir = Path(args.base_dir).absolute()
    return args

def main():
    args = get_args()
    dirs = natsorted(list(filter(lambda x: x.is_dir(), args.base_dir.glob("frame*"))), key=lambda p: p.name)
    assert len(dirs) > 0
    npz_loss_files = [dir / "loss.npz" for dir in dirs]
    assert sum([not x.exists() for x in npz_loss_files]) == 0
    npz_files = [np.load(x, allow_pickle=True)["arr_0"].item() for x in npz_loss_files]

    _range = range(len(npz_files))
    keys = npz_files[0].keys()
    firsts = {k: [npz_files[i][k][0] for i in _range] for k in keys}
    seconds = {k: [npz_files[i][k][1] for i in _range] for k in keys}
    lasts = {k: [npz_files[i][k][-1] for i in _range] for k in keys}

    fig, ax = plt.subplots(2, 2, figsize=(12, 10))
    for i, k in enumerate(keys):
        ax[i // 2][i % 2].plot(_range, firsts[k], label="first iteration")
        ax[i // 2][i % 2].plot(_range, seconds[k], label="second")
        ax[i // 2][i % 2].plot(_range, lasts[k], label="last")
        ax[i // 2][i % 2].set_title(k)
        means = [np.mean(firsts[k]), np.mean(seconds[k]), np.mean(lasts[k])]
        str_means = [f"{x:.3f}" for x in means]
        ax[i // 2][i % 2].set_title(f"{k}. Means: {str_means}")

    handles, labels = ax[1][1].get_legend_handles_labels()
    fig.legend(handles, labels, loc="upper right")
    plt.suptitle(args.title)
    plt.savefig("plot.png")
    plt.close()

if __name__ == "__main__":
    main()