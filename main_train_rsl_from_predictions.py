import yaml
from pathlib import Path
from argparse import ArgumentParser
import numpy as np
import torch as tr
from torch import optim
from nwutils.pytorch_lightning import PLModule
from pytorch_lightning import Trainer
from pytorch_lightning.callbacks import Callback, ModelCheckpoint
from torchinfo import summary
from ngclib_cv.data import build_plot_function
from media_processing_lib.image import image_write
from ngclib.utils import load_get_nodes_module

from reader import get_loader, split_input, get_reader
from model import get_model


class PlotCallback(Callback):
    def __init__(self, input_nodes, output_node):
        self.input_nodes = input_nodes
        self.output_node = output_node
        self.plot_fns = [build_plot_function(x) for x in [*self.input_nodes, self.output_node, self.output_node]]
        # self.titles = [*[x.name for x in self.input_nodes], self.output_node.name, "GT"]

    def do_plot(self, split_data, out_path):
        Path(out_path).mkdir(exist_ok=True, parents=True)
        MB = len(split_data[0])
        for i in range(MB):
            x_images = [plot_fn(x[i]) for (plot_fn, x) in zip(self.plot_fns, split_data)]
            stack = np.concatenate(x_images, axis=1)
            image_write(stack, f"{out_path}/{i}.png")

    def on_validation_batch_end(self, trainer, pl_module, outputs, batch, batch_idx, dataloader_idx):
        if batch_idx != 0:
            return
        png_path = Path(trainer.default_root_dir) /\
            f"lightning_logs/version_{trainer.logger.version}/pngs/{trainer.current_epoch}"
        with tr.no_grad():
            y = pl_module.forward(tr.FloatTensor(batch["data"]).to(pl_module.device)).to("cpu").numpy()
        gt = batch["labels"][self.output_node]
        x_split = [*split_input(self.input_nodes, batch["data"]), y, gt]
        self.do_plot(x_split, png_path)

def get_args():
    parser = ArgumentParser()
    parser.add_argument("--graph_cfg_path", required=True)
    parser.add_argument("--train_cfg_path", required=True)
    parser.add_argument("--graph_nodes_path", required=True)
    parser.add_argument("--train_set_path", required=True)
    parser.add_argument("--validation_set_path", required=True)
    args = parser.parse_args()
    args.graph_nodes_path = Path(args.graph_nodes_path).absolute()
    return args

def main():
    args = get_args()
    graph_cfg = yaml.safe_load(open(args.graph_cfg_path, "r"))
    train_cfg = yaml.safe_load(open(args.train_cfg_path, "r"))
    nodes = load_get_nodes_module(args.graph_nodes_path, "getNodes")(graph_cfg)
    input_nodes = sorted(set(nodes).difference(graph_cfg["inputNodes"]), key=lambda node: node.name)
    output_node = list(set(nodes).difference(input_nodes))[0]
    train_loader = get_loader(get_reader(args.train_set_path, nodes, input_nodes, train_cfg, use_augmentation=True),
                              train_cfg, randomize=True, debug=False)
    val_loader = get_loader(get_reader(args.validation_set_path, nodes, input_nodes, train_cfg, use_augmentation=True),
                            train_cfg, randomize=False, debug=False)
    assert len(input_nodes) == 1
    model = get_model(input_node=input_nodes[0], output_node=output_node)

    pl_model = PLModule(model)
    print(pl_model.summary(input_size=next(iter(train_loader))["data"].shape))
    pl_model.optimizer = optim.Adam(model.parameters(), lr=0.001)
    pl_model.criterion_fn = lambda y, gt: ((y - gt["rgb"])**2).mean()
    callbacks = [PlotCallback(input_nodes, output_node), ModelCheckpoint(monitor="val_loss", save_last=True)]
    Trainer(max_epochs=train_cfg["numEpochs"], callbacks=callbacks, gpus=1) \
        .fit(pl_model, train_dataloaders=train_loader, val_dataloaders=val_loader)

if __name__ == "__main__":
    main()

