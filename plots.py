from reader import split_input
import matplotlib.pyplot as plt
import numpy as np
from functools import partial
from nwgraph.node import Node
from ngclib_cv.data import build_plot_function
from ngclib_cv.data.plot_functions import default
from media_processing_lib.image import image_add_title, image_resize, image_write
from media_processing_lib.collage_maker import collage_fn
from media_processing_lib.video import MPLVideo
from pathlib import Path
from typing import List, Dict

def do_video_plot(res_x, res_y, res_loss, gt, input_nodes, output_node, output_dir):
    """Create a video of the input evolution w.r.t grad of gt and a plot of loss w.r.t reconstruction"""
    rgb_loss = res_loss["rgb"]
    rgb = gt["rgb"]
    # plot_fns = [buildNodePlotFn(x) for x in
    #     [*input_nodes, output_node, output_node,
    #      *input_nodes, output_node, output_node,
    #      output_node, output_node, output_node]]

    d, n, s = split_input(input_nodes, res_x)
    N = len(res_y)
    gt = np.repeat(rgb[None], N, axis=0)
    d0 = np.repeat(d[0:1], N, axis=0)
    n0 = np.repeat(n[0:1], N, axis=0)
    s0 = np.repeat(s[0:1], N, axis=0)
    y0 = np.repeat(res_y[0:1], N, axis=0)

    d_diff = np.abs(d0 - d).sum(-1)
    n_diff = np.abs(n0 - n).sum(-1)
    s_diff = np.abs(s0 - s).sum(-1)
    d_diff_percent = d_diff.max(-1).max(-1) / d.max(-1).max(-1).max(-1) * 100
    n_diff_percent = n_diff.max(-1).max(-1) / n.max(-1).max(-1).max(-1) * 100
    s_diff_percent = s_diff.max(-1).max(-1) / s.max(-1).max(-1).max(-1) * 100

    d_diff_px = d_diff / d_diff.max(-1).max(-1)[..., None][..., None]
    d_diff_px[np.isnan(d_diff_px)] = 0
    n_diff_px = n_diff / n_diff.max(-1).max(-1)[..., None][..., None]
    n_diff_px[np.isnan(n_diff_px)] = 0
    s_diff_px = s_diff / s_diff.max(-1).max(-1)[..., None][..., None]
    s_diff_px[np.isnan(s_diff_px)] = 0

    zeros = d[..., 0] * 0
    rgb_zeros = np.repeat(zeros[..., None], 3, axis=-1)
    d_diff_px = np.stack([d_diff_px, zeros, zeros], axis=-1)
    n_diff_px = np.stack([n_diff_px, zeros, zeros], axis=-1)
    s_diff_px = np.stack([s_diff_px, zeros, zeros], axis=-1)
    data = [d0, n0, s0, y0, gt,
            d, n, s, res_y, rgb_zeros,
            d_diff_px, n_diff_px, s_diff_px]
    orders = [*input_nodes, output_node, output_node,
         *input_nodes, output_node, output_node,
         output_node, output_node, output_node]
    rc = (3, 5)

    # orders = [*input_nodes, output_node, output_node,
    #      *input_nodes, output_node]
    # data = [d0, n0, s0, y0, gt, d, n, s, res_y]
    # rc = (2, 5)
    plot_fns = [build_plot_function(x) for x in orders]
    img_data = []
    for i in range(N):
        images = [plot_fn(x[i]) for (plot_fn, x) in zip(plot_fns, data)]
        images = np.array(images)
        collage = collage_fn(images, rowsCols=rc)
        collage = image_resize(collage, width=1920, height=None)
        title = f"Iteration {i}. Loss: {rgb_loss[i]:.5f}. Diffs(%): [{d_diff_percent[i]:.3f}, " \
                f"{s_diff_percent[i]:.3f}, {n_diff_percent[i]:.3f}]"
        collage = image_add_title(collage, text=title, size_px=40)

        img_data.append(collage)
    img_data = np.array(img_data)
    video = MPLVideo(img_data, fps=5)
    video.write(output_dir / "vid.mp4")
    print(f"Video stored at '{output_dir}/vid.mp4'")

def do_figure_plot(res_loss, output_dir, lr):
    """
    Do plots of each of the input nodes (depth, normals, semantic), as well as the output (rgb). Relative to the
    first result before any optimization, too.
    """

    fig, ax = plt.subplots(2, len(res_loss), figsize=(3 * len(res_loss), 10))
    plt.suptitle(f"frame: {str(output_dir).split(' ')[-1]} lr: {lr}")
    fig.tight_layout(pad=4.0)
    X = np.arange(len(res_loss[list(res_loss.keys())[0]]))

    for i, (node_name, node_values) in enumerate(res_loss.items()):
        rel_node_values = (node_values / node_values[0])
        ax[0, i].plot(X, node_values)
        ax[0, i].set_title(node_name)
        ax[0, i].ticklabel_format(useOffset=False)
        ax[1, i].plot(X, rel_node_values)
        ax[1, i].set_title(f"{node_name} relative")

    out_file = output_dir / f"loss_lr_{lr}.png"
    plt.savefig(out_file)
    plt.close()
    print(f"Saved at {out_file}")

def do_images_plot(res_x: np.ndarray, res_y: np.ndarray, gt: Dict[str, np.ndarray], input_nodes: List[Node],
                   output_dir: Path, export_individual: bool, export_collage: bool):
    """
    Export the first, second and last iteration as images.
    If export_individual is True, export as {output_dir}_{node}_first/second/last.png.
    if export_collage is True, export as {output_dir}_collage.png
    """
    assert export_individual + export_collage >= 1, "At least one must be true."
    split_pred = split_input(input_nodes, res_x)
    pred_nodes = dict(zip(input_nodes, split_pred))
    pred_rgb = res_y
    # semantic_pred_rens => semantic_gt
    gt_nodes = {node: gt[f"{str(node).split('_')[0] + '_gt'}"] for node in input_nodes}
    gt_rgb = gt["rgb"]

    assert len(split_pred[0]) == len(pred_rgb), f"{len(split_pred[0])} vs {len(pred_rgb)}"
    assert len(pred_rgb) >= 2
    names = ["first", "second", "last"]
    # depth, normals, semantic, RGB
    out_nodes = [*input_nodes, "rgb"]
    plot_fns = [*[build_plot_function(node) for node in input_nodes], partial(default, node=None)]
    collage_imgs = []

    # First, output the first, 2nd and last iteration predictions for all nodes (input and reeconstructed)
    for ix, name in zip([0, 1, -1], names):
        pred_ix = [*[pred_nodes[k][ix] for k in input_nodes], pred_rgb[ix]]
        for pred, plot_fn, node_name in zip(pred_ix, plot_fns, out_nodes):
            img = plot_fn(pred)
            if export_individual:
                out_file = output_dir / f"{node_name}_{name}.png"
                image_write(img, out_file)
            collage_imgs.append(img)

    # Then, output the GT images
    gt_out = [*[gt_nodes[node] for node in input_nodes], gt_rgb]
    for plot_fn, x, node_name in zip(plot_fns, gt_out, out_nodes):
        img = plot_fn(x)
        if export_individual:
            out_file = output_dir / f"{node_name}_gt.png"
            image_write(img, out_file)
        collage_imgs.append(img)

    # Finally, do the collage plot as 4 rows and output_nodes + rgb cols
    if export_collage:
        rows_cols = 4, len(collage_imgs) // 4
        collage = image_resize(collage_fn(collage_imgs, rows_cols), height=1080, width=None)
        image_write(collage, output_dir / "collage.png")

        rows_cols = 2, len(collage_imgs) // 4
        collage_imgs = collage_imgs[0 : 4] + collage_imgs[-4 :]
        collage = image_resize(collage_fn(collage_imgs, rows_cols), height=1080, width=None)
        image_write(collage, output_dir / "collage_first.png")
    
    print(f"Stored images at {output_dir}")

def do_plots(res_x, res_y, res_loss, gt, input_nodes, output_node, output_dir, lr):
    output_dir = Path(output_dir)
    output_dir.mkdir(exist_ok=True, parents=True)
    # do_video_plot(res_x, res_y, res_loss, gt, input_nodes, output_node, output_dir)
    do_figure_plot(res_loss, output_dir, lr)
    do_images_plot(res_x, res_y, gt, input_nodes, output_dir, export_individual=False, export_collage=True)
    np.savez(output_dir / "loss.npz", res_loss)
