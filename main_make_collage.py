from media_processing_lib.video import MPLVideo
from media_processing_lib.image import image_resize, image_add_title
from pathlib import Path
from argparse import ArgumentParser
from tqdm import tqdm
from natsort import natsorted
from functools import partial

def get_args():
    parser = ArgumentParser()
    parser.add_argument("--plots_dir", required=True)
    parser.add_argument("--output_file")
    parser.add_argument("--max_height", type=int, default=1080)
    parser.add_argument("--fps", type=float, default=2)
    args = parser.parse_args()
    args.plots_dir = Path(args.plots_dir).absolute()
    if args.output_file is None:
        args.output_file = args.plots_dir / "video.mp4"
    args.output_file = Path(args.output_file).absolute()
    return args

def apply_fn(video, i, dirs, max_height):
    image = video[i]
    height = max(max_height, image.shape[0])
    image = image_resize(image, height=max_height, width=None)
    image = image_add_title(image, text=dirs[i].name, size_px=100)
    return image

def main():
    args = get_args()
    dirs = natsorted(list(filter(lambda x: x.is_dir(), args.plots_dir.glob("frame*"))), key=lambda p: p.name)
    assert len(dirs) > 0
    image_paths = [dir / "collage.png" for dir in dirs]
    for image_path in tqdm(image_paths, desc="Reading predicitons"):
        assert image_path.exists(), f"'{image_path}' does not exist"
    video = MPLVideo(image_paths, fps=args.fps)
    video.write(args.output_file, partial(apply_fn, dirs=dirs, max_height=args.max_height))
    print(f"Saved video at '{args.output_file}' at {args.fps} fps")

if __name__ == "__main__":
    main()
