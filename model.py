from torch import nn
from nwgraph.node import Node
from typing import List

def get_model(input_node: List[Node], output_node: Node) -> nn.Module:
    encoder = input_node.get_encoder(output_node)
    decoder = output_node.get_decoder(input_node)
    model = nn.Sequential(encoder, decoder)
    return model
