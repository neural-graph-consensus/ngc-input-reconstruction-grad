import numpy as np
from pathlib import Path
from natsort import natsorted
from tqdm import tqdm

path = Path("/scratch/nvme0n1/ngc/ngc-input-reconstruction-grad/datasets/slanic/validation_set/semantic_gt")
path_out = Path("/scratch/nvme0n1/ngc/ngc-input-reconstruction-grad/datasets/slanic/validation_set/semantic_gt_forest")
files_paths = natsorted([x for x in path.glob("*.npz")], key=lambda path: path.name)
files = [np.load(x) for x in tqdm(files_paths)]
path_out.mkdir(exist_ok=True)

for file, file_path in tqdm(zip(files, files_paths)):
    file = file["arr_0"]
    file = (file == 1)
    np.savez(path_out / file_path.name, file)
