import pandas as pd
import numpy as np
import sys

file_names = ["loss_slanic_min.npz","loss_slanic_min_depth_hole_5.npz","loss_slanic_min_depth_hole_15.npz","loss_slanic_min_depth_hole_30.npz","loss_slanic_min_normals_hole_5.npz","loss_slanic_min_normals_hole_15.npz","loss_slanic_min_normals_hole_30.npz","loss_slanic_min_semantic_hole_5.npz","loss_slanic_min_semantic_hole_15.npz","loss_slanic_min_semantic_hole_30.npz"]
names = [x.split(".")[0][12:] for x in file_names]
dir = sys.argv[1]
items = []
keys = ["rgb", "depth_pred_rens", "normals_pred_rens", "semantic_pred_rens"]
res = {name: [] for name in names}
for name, file_name in zip(names, file_names):
    item = np.load(f"{dir}/{file_name}", allow_pickle=True)["arr_0"].item()
    _res = []
    for k in keys:
        _res.append(f"{item[k][0]:.3f}")
        _res.append(f"{item[k][-1]:.3f}")
    res[name] = _res
res = pd.DataFrame(res).T
res.columns = ["RGB start", "RGB end", "Depth start", "Depth end", "Normals start", "Normals end", "Semantic start", "Semantic end"]
res.to_csv(f"{dir}.csv")
