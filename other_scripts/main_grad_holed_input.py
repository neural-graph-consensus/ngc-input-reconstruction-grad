from main_grad_input import *
from reader import HoledReader

def get_args():
    parser = ArgumentParser()
    parser.add_argument("--graph_cfg_path", required=True)
    parser.add_argument("--graph_nodes_path", required=True)
    parser.add_argument("--weights_path", required=True)
    parser.add_argument("--dataset_path", required=True)
    parser.add_argument("--output_dir", required=True)
    parser.add_argument("--lr", type=float, default=0.01)
    parser.add_argument("--ix", type=int, nargs="+")
    parser.add_argument("--max_iters", type=int, default=3)
    parser.add_argument("--eps", type=float, default=1e-5)
    parser.add_argument("--depth_hole", type=int)
    parser.add_argument("--semantic_hole", type=int)
    parser.add_argument("--normals_hole", type=int)
    parser.add_argument("--hole_percent", type=int, nargs="+")
    args = parser.parse_args()
    args.graph_nodes_path = Path(args.graph_nodes_path).absolute()
    args.dataset_path = Path(args.dataset_path).absolute()
    args.output_dir = Path(args.output_dir).absolute()
    assert len(args.hole_percent) == 2
    return args

def main():
    args = get_args()
    graph_cfg = yaml.safe_load(open(args.graph_cfg_path, "r"))
    # Load nodes from ngc-input-reconstruction module and input_nodes are the opposite of the regular ones from the
    #  standard NGC models (RGB->D, RGB->S, RGB->N becomes N->RGB, S->RGB, D->RGB, so RGB is output node now.)
    nodes = load_get_nodes_module(args.graph_nodes_path, "getNodes")(graph_cfg)
    input_nodes = sorted(set(nodes).difference(graph_cfg["inputNodes"]), key=lambda node: node.name)

    reader = HoledReader(hole_node_percentages={"semantic_pred_rens": args.semantic_hole,
                                                "depth_pred_rens": args.depth_hole,
                                                "normals_pred_rens": args.normals_hole},
                         hole_percent=args.hole_percent, path=args.dataset_path,
                         nodes=nodes, gt_nodes=input_nodes, transforms_fn=get_data_transform)
    in_dims = sum([x.num_dims for x in input_nodes])
    model = get_model(in_dims=in_dims, out_dims=3, nodes_path=str(args.graph_nodes_path.parent/"nodes"))
    PLModule(model).load_state_dict(tr.load(args.weights_path, map_location="cpu")["state_dict"])
    model.eval().to(device)

    run_optimization_on_reader(model, reader, args.ix, args.lr, args.eps, args.max_iters, args.output_dir)

if __name__ == "__main__":
    main()
