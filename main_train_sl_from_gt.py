import yaml
from pathlib import Path
from argparse import ArgumentParser
from torch import optim
from nwutils.pytorch_lightning import PLModule, PlotCallback
from pytorch_lightning import Trainer
from pytorch_lightning.callbacks import ModelCheckpoint
from pytorch_lightning.loggers import TensorBoardLogger
from ngclib_cv.data import build_plot_function
from media_processing_lib.image import image_write
from media_processing_lib.collage_maker import collage_fn
from ngclib.utils import load_get_nodes_module

from reader import get_loader, get_reader
from model import get_model
from functools import partial

def plot_fn(x, y, gt, out_dir, input_node, output_node):
    mb = len(x)
    for i in range(mb):
        x_img = build_plot_function(input_node)(x[i])
        y_img = build_plot_function(output_node)(y[i])
        gt_img = build_plot_function(output_node)(gt[output_node][i])
        collage = collage_fn([x_img, y_img, gt_img], (3, 1))
        image_write(collage, out_dir / f"{i}.png")

def get_args():
    parser = ArgumentParser()
    parser.add_argument("--graph_cfg_path", required=True)
    parser.add_argument("--train_cfg_path", required=True)
    parser.add_argument("--graph_nodes_path", required=True)
    parser.add_argument("--train_set_path", required=True)
    parser.add_argument("--validation_set_path", required=True)
    parser.add_argument("--experiment_name")
    args = parser.parse_args()
    args.graph_nodes_path = Path(args.graph_nodes_path).absolute()
    return args

def main():
    args = get_args()
    graph_cfg = yaml.safe_load(open(args.graph_cfg_path, "r"))
    train_cfg = yaml.safe_load(open(args.train_cfg_path, "r"))
    nodes = load_get_nodes_module(args.graph_nodes_path, "getNodes")(graph_cfg)
    input_nodes = list(filter(lambda node: node.name in graph_cfg["inputNodes"], nodes))
    assert len(input_nodes) == 1
    assert len(nodes) == 2
    output_node = list(filter(lambda node: node.name not in graph_cfg["inputNodes"], nodes))[0]
    train_loader = get_loader(get_reader(args.train_set_path, nodes, input_nodes, train_cfg, use_augmentation=True),
                              train_cfg, randomize=True, debug=False)
    val_loader = get_loader(get_reader(args.validation_set_path, nodes, input_nodes, train_cfg, use_augmentation=True),
                            train_cfg, randomize=False, debug=False)
    model = get_model(input_nodes[0], output_node)
    pl_model = PLModule(model)
    print(pl_model.summary(input_size=next(iter(train_loader))["data"].shape))
    pl_model.optimizer = optim.Adam(model.parameters(), lr=0.001)
    pl_model.criterion_fn = lambda y, gt: output_node.get_node_criterion()(y, gt[output_node.name]).mean()
    callbacks = [ModelCheckpoint(monitor="val_loss", save_last=True),
                 PlotCallback(partial(plot_fn, input_node=input_nodes[0], output_node=output_node))]
    pl_logger = TensorBoardLogger("lightning_logs/", version=args.experiment_name)
    Trainer(max_epochs=train_cfg["numEpochs"], callbacks=callbacks, gpus=1, logger=pl_logger) \
        .fit(pl_model, train_dataloaders=train_loader, val_dataloaders=val_loader)

if __name__ == "__main__":
    main()

