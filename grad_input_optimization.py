from typing import List, Tuple
import torch as tr
import numpy as np
from torch.nn.functional import binary_cross_entropy
from torch import nn
from ngclib_cv.nodes import Depth, Semantic, Normal, RGB
from nwutils.torch import tr_get_data, tr_to_device
from pathlib import Path

from reader import Reader, split_input
from plots import do_plots

def l1(y: tr.Tensor, gt: tr.Tensor, *args, **kwargs) -> float:
    return float((y - gt).abs().mean().to("cpu").detach())

def l1_px(y, gt, node):
    return l1(y, gt) * 255

def l1_metric(y, gt, node: Depth):
    return l1(y, gt) * node.hyper_parameters["max_depth_meters"]

def bce(y, gt, node):
    return binary_cross_entropy(y, gt).detach().to("cpu").numpy().item()

def _update_metrics(res, x, y, gt, input_nodes):
    # TODO: RGB hack all over the place.
    node_type_to_metric_fn = {
        RGB: l1_px,
        Depth: l1_metric,
        Semantic: bce,
        Normal: l1
    }

    if res is None:
        res = {}
        for node in input_nodes:
            res[node.name] = []
        res["rgb"] = []

    res["rgb"] += [l1_px(y, gt["rgb"], None)]
    x_split = dict(zip(input_nodes, split_input(input_nodes, x[0])))
    for node in input_nodes:
        metric_fn = None
        for _type, fn in node_type_to_metric_fn.items():
            if isinstance(node, _type):
                metric_fn = fn
                break
        assert metric_fn is not None
        # semantic_pred_rens => semantic_gt
        gt_name = str(node).split("_")[0] + "_gt"
        _x, _gt = x_split[node], gt[gt_name]
        res[node.name] += [metric_fn(_x, _gt, node)]
    return res

def do_one_optimization(model: tr.nn.Module, input_nodes: List, x: tr.Tensor,
                        gt: tr.Tensor, lr: float, eps: float, max_iters: int = 5000) -> Tuple[np.ndarray]:
    """Given a (x=(d, n, s), gt=(rgb)) pair, optimizer the input (x) w.r.t the L=(y - rgb) error for n steps"""
    new_x = x.requires_grad_()
    res_x = []
    res_y = []
    prev_loss = 100
    i = 1

    res_metrics = None

    while True:
        res_x.append(new_x.to("cpu").detach().numpy()[0])
        y = model.forward(new_x)[0]
        res_y.append(y.to("cpu").detach().numpy())
        # L1 input nodes & RGB metrics
        res_metrics = _update_metrics(res_metrics, new_x, y, gt, input_nodes)

        # Loss & grad
        loss = ((y - gt["rgb"])**2).mean()
        str_res = {k: f"{v[-1]:5f}" for k, v in res_metrics.items()}
        print(f"Iteration {i}. Loss {str_res}")
        loss.backward()
        new_x = (new_x - lr * new_x.grad).detach().clip(0, 1).requires_grad_()

        if (loss - prev_loss).abs() < eps:
            print(f"Stopped at iter {i} because (loss) {loss:.5f} - (prev) {prev_loss:.5f} < (eps) {eps}")
            break
        if i >= max_iters:
            print(f"Stopped at max iter {max_iters}")
            break
        prev_loss = loss.detach()
        i += 1

    print(f"Finished at {i} iterations")
    # convert everything to numpy arrays
    return np.array(res_x), np.array(res_y), {k : np.array(res_metrics[k]) for k in res_metrics}

def run_optimization_on_reader(model: nn.Module, reader: Reader, indexes: List[int], lr: float,
                               eps: float, max_iters: int, output_dir: Path):
    """
    Run the input optimization loop on a model, given a dataset reader and some indexes in that reader. We get a
    learning rate, epsilon between two iterations for stopping criterion, a maximum number of iterations as well as an
    output path, where we'd store the plots.
    """
    if indexes is None:
        print(f"no indexes offered, looping all dataset one by one ({len(reader)} items)")
        indexes = range(len(reader))

    device = next(model.parameters()).device
    # TODO: hack for rgb input node
    output_node = reader.nodes[reader.nodes.index("rgb")]
    print(f"Optimizing the output node '{output_node}' on {len(indexes)} indexes, lr {lr:.3f}, "
          f"eps {eps:.3f}, max_iters: {max_iters}. Saving at '{output_dir}'")

    for ix in indexes:
        item = reader[ix]
        data, labels = item["data"], item["labels"]
        data = np.concatenate([data[k] for k in reader.gt_nodes], axis=-1)

        gt = tr_to_device(tr_get_data(labels), device)
        x = tr.from_numpy(data[None]).to(device)
        res_x, res_y, res_loss = do_one_optimization(model, reader.gt_nodes, x, gt, lr, eps, max_iters)
        frame_output_dir = output_dir / f"frame {ix}"
        do_plots(res_x, res_y, res_loss, labels, reader.gt_nodes, output_node, frame_output_dir, lr)
