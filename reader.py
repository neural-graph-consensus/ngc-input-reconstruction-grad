from pathlib import Path
from typing import List, Dict, Callable, Tuple
import numpy as np
from overrides import overrides
from torch.utils.data import DataLoader
from scipy.special import softmax
from nwgraph import Node, ClassificationNode
from ngclib.readers import NGCNpzReader
from ngclib.utils import NGCAugmentation, GeneralPrototype
from nwgraph.node import MapNode
from ngclib.logger import logger
from ngclib_cv.data import get_augmentation, get_data_transform

class Reader(NGCNpzReader):
    def __init__(self, path: Path, nodes: List[Node], gt_nodes: List[Node], transforms_fn: Dict[Node, Callable],
                 general_augmentation: List[GeneralPrototype] = None):
        gt_nodes = sorted(gt_nodes, key=lambda node: node.name)
        transforms = {node: transforms_fn(node) for node in nodes}
        super().__init__(path, nodes, gt_nodes, transforms)
        self.augmentation = NGCAugmentation(nodes, general_augmentation, None)

    def get_node_item(self, node: Node, index: int) -> np.ndarray:
        item = super().get_node_item(node, index)
        if isinstance(node, ClassificationNode) and item.shape[-1] == len(node.classes) and len(node.classes) > 1:
            sum_last_dim = (np.abs(item.sum(axis=-1) - 1) > 1e-3).sum()
            if sum_last_dim > 0:
                item = softmax(item, axis=-1)
        transformedItem = self.transforms[node.name](item, node)
        return transformedItem

    @overrides
    def merge_fn(self, x):
        """
        EdgeReader will not behave like the graph reader. Data is given as a dict, while labels as a regular
        array. Edges will have to understand this format for training.
        """
        res = super().merge_fn(x)
        all_nodes_data = res["labels"]
        all_nodes_data = self.augmentation(all_nodes_data)
        labels = {k: np.array([y["labels"][k] for y in x], dtype=np.float32) for k in x[0]["labels"].keys()}
        # gtNodes are already sorted
        data = np.concatenate([labels[k] for k in self.gt_nodes], axis=-1)
        return {"data": data, "labels": labels}

class HoledReader(Reader):
    """
    Extends the basic edge reader with one change: adds a random hole to each input node with probability
    hole_node_percentages. The hole's shape is given by hole_percent, representing (h, w) percentages w.r.t image
    shape.
    """
    def __init__(self, hole_node_percentages: Dict[str, float], hole_percent: Tuple[float, float], *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.hole_node_percentages = hole_node_percentages
        self.hole_percent = hole_percent
    
    @overrides
    def get_node_item(self, node: Node, index: int) -> np.ndarray:
        item = super().get_node_item(node, index)
        if isinstance(node, ClassificationNode) and item.shape[-1] == len(node.classes):
            sum_last_dim = (np.abs(item.sum(axis=-1) - 1) > 1e-3).sum()
            if sum_last_dim > 0:
                item = softmax(item, axis=-1)
        transformedItem = self.transforms[node.name](item, node)
        if node not in self.gt_nodes:
            return transformedItem
        if np.random.random() > self.hole_node_percentages[node]:
            return transformedItem
        h = self.hole_percent[0] * transformedItem.shape[0] // 100
        w = self.hole_percent[1] * transformedItem.shape[1] // 100
        h_range = [0, transformedItem.shape[0] - h]
        w_range = [0, transformedItem.shape[1] - w]
        np.random.seed(index)
        h_start = np.random.choice(np.arange(*h_range))
        w_start = np.random.choice(np.arange(*w_range))
        transformedItem[h_start : h_start + h, w_start : w_start + w] = 0
        return transformedItem


def get_reader(path: Path, nodes: List[Node], gt_nodes: List[Node], train_cfg: Dict,
               use_augmentation: bool) -> NGCNpzReader:
    """Creates a ngc npz reader based on a path, list of (gt) nodes, train cfg & augmentation flag"""
    general_augmentation = []
    if use_augmentation is True and "augmentation" in train_cfg and "general" in train_cfg["augmentation"]:
        for name, augmentation_args in train_cfg["augmentation"]["general"].items():
            logger.info(f"Using augmentation '{name}'")
            general_augmentation.append(get_augmentation(name, **augmentation_args))
    reader = Reader(path=path, nodes=nodes, gt_nodes=gt_nodes,
                    transforms_fn=get_data_transform, general_augmentation=general_augmentation)
    return reader

def get_loader(reader: NGCNpzReader, train_cfg: Dict, randomize: bool = True, debug: bool = False) -> DataLoader:
    """Gets a DataLoader from an NGCNpzReader, an edge and a train cfg"""
    num_workers = train_cfg["num_workers"] if "num_workers" in train_cfg else 0
    loader = reader.to_data_loader(batch_size=train_cfg["batchSize"], num_workers=num_workers,
                                   randomize=randomize, debug=debug, seed=train_cfg["seed"])
    return loader

def split_input(nodes: List[MapNode], input: np.ndarray) -> List[np.ndarray]:
    """
    Given a concatenated array with multiple nodes' data on the last dimesnion, return the split array with one item
    for each node.
    """
    res = []
    current = 0
    for node in nodes:
        current_data = input[..., current : current + node.num_dims]
        current += node.num_dims
        res.append(current_data)
    return res
